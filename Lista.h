//
//  Lista.h
//  Lista
//
//  Created by Eiji Adachi Medeiros Barbosa on 31/03/16.
//  Copyright (c) 2016 Eiji Adachi Medeiros Barbosa. All rights reserved.
//

struct tpNo
{
    int valor;
    struct tpNo* proximo;
    struct tpNo* anterior;
};

typedef tpNo * LIS_No;

struct tpLista
{
    LIS_No cabeca;
    LIS_No cauda;
    int tamanho;
};

typedef tpLista * LIS_Lista;


LIS_Lista LIS_CriarLista();

bool LIS_InserirInicio(LIS_Lista, int);
bool LIS_InserirFim(LIS_Lista, int);
bool LIS_Inserir(LIS_Lista, int, int);

int LIS_RemoverInicio(LIS_Lista);
int LIS_RemoverFim(LIS_Lista);
int LIS_Remover(LIS_Lista, int);

int LIS_PegarValor(LIS_Lista, int);

void LIS_ImprimirLista(LIS_Lista);

bool LIS_Verificar(LIS_Lista);
