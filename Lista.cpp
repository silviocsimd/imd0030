//
//  Lista.cpp
//  Lista
//
//  Created by Eiji Adachi Medeiros Barbosa on 31/03/16.
//  Copyright (c) 2016 Eiji Adachi Medeiros Barbosa. All rights reserved.
//

#include <stdlib.h>
#include <iostream>
#include "Lista.h"

#define VALOR_QUALQUER -10000

LIS_No LIS_CriarNo(int);
bool LIS_DestruirNo(LIS_No);

LIS_Lista LIS_CriarLista()
{
    //LIS_Lista lista = (LIS_Lista)malloc(sizeof(tpLista));
    LIS_Lista lista = new tpLista;
    if( lista == NULL )
    {
        return NULL;
    }
    lista->tamanho = 0;
    lista->cabeca = LIS_CriarNo(VALOR_QUALQUER);
    lista->cauda  = LIS_CriarNo(VALOR_QUALQUER);

    lista->cabeca->proximo  = lista->cauda;
    lista->cabeca->anterior = NULL;

    lista->cauda->proximo  = NULL;
    lista->cauda->anterior = lista->cabeca;

    return lista;
}

int LIS_PegarValor(LIS_Lista lista, int indice)
{
    if( indice > lista->tamanho )
    {
        return -1;
    }

    int cont = 1;
    for(LIS_No i = lista->cabeca->proximo; i != lista->cauda; i=i->proximo)
    {
        if( cont == indice )
        {
            return i->valor;
        }
        cont++;
    }

    // Não deveria chegar aqui. Se chegou, houve erro.
    return -1;
}

bool LIS_InserirInicio(LIS_Lista lista, int v)
{
    LIS_No no = LIS_CriarNo(v);
    if( no == NULL )
    {
        return false;
    }

    LIS_No primeiro = lista->cabeca->proximo;

    primeiro->anterior = no;
    no->proximo = primeiro;

    lista->cabeca->proximo = no;
    no->anterior = lista->cabeca;

    lista->tamanho++;

    return true;
}

bool LIS_InserirFim(LIS_Lista lista, int v)
{
    LIS_No no = LIS_CriarNo(v);
    if( no == NULL )
    {
        return false;
    }

    LIS_No ultimo = lista->cauda->anterior;

    ultimo->proximo = no;
    no->anterior = ultimo;

    no->proximo = lista->cauda;
    lista->cauda->anterior = no;

    lista->tamanho++;

    return true;
}

bool LIS_Inserir(LIS_Lista lista, int v, int indice)
{
    if( indice > lista->tamanho+1 || indice <= 0)
    {
        return false;
    }

    int cont = 1;

    LIS_No i;
    for(i = lista->cabeca->proximo; i != lista->cauda; i = i->proximo)
    {
        if(cont == indice)
        {
            break;
        }
        cont++;
    }

    LIS_No novo = LIS_CriarNo(v);

    novo->proximo = i;
    novo->anterior = i->anterior;

    i->anterior->proximo = novo;
    i->anterior = novo;

    lista->tamanho++;

    return true;
}

int LIS_RemoverInicio(LIS_Lista lista)
{
    if( lista->tamanho == 0 )
    {
        return -1;
    }

    LIS_No primeiro = lista->cabeca->proximo;

    lista->cabeca->proximo = primeiro->proximo;
    primeiro->proximo->anterior = lista->cabeca;

    int valor = primeiro->valor;

    bool destruiuNo = LIS_DestruirNo(primeiro);

    if( destruiuNo )
    {
        lista->tamanho--;
        return valor;
    }

    return -1;
}

int LIS_RemoverFim(LIS_Lista lista)
{
    if( lista->tamanho == 0 )
    {
        return -1;
    }

    LIS_No ultimo = lista->cauda->anterior;

    lista->cauda->anterior = ultimo->anterior;
    ultimo->anterior->proximo = lista->cauda;

    int valor = ultimo->valor;

    bool destruiuNo = LIS_DestruirNo(ultimo);

    if( destruiuNo )
    {
        lista->tamanho--;
        return valor;
    }

    return -1;
}

int LIS_Remover(LIS_Lista lista, int indice)
{
    if( indice <= 0 || indice > lista->tamanho )
    {
        return -1;
    }

    int cont = 1;
    for(LIS_No i = lista->cabeca->proximo; i != lista->cauda; i=i->proximo)
    {
        if(cont == indice)
        {
            // i é o elemento a ser removido
            i->anterior->proximo = i->proximo;
            i->proximo->anterior = i->anterior;

            int valor = i->valor;

            bool destruiu = LIS_DestruirNo(i);
            if(destruiu)
            {
                lista->tamanho--;
                return valor;
            }
            else
            {
                break;
            }
        }

        cont++;
    }

    return -1;
}

bool LIS_Verificar(LIS_Lista lista)
{
    if( lista->cabeca == NULL )
    {
        return false;
    }

    if( lista->cauda == NULL )
    {
        return false;
    }

    if( lista->cabeca->anterior != NULL )
    {
        return false;
    }


    if( lista->cauda->proximo != NULL )
    {
        return false;
    }

    if(lista->tamanho == 0)
    {
        if( lista->cabeca->proximo != lista->cauda )
        {
            return false;
        }

        if( lista->cabeca != lista->cauda->anterior )
        {
            return false;
        }
    }
    else
    {
        for(LIS_No i = lista->cabeca->proximo; i != lista->cauda; i=i->proximo)
        {
            if( i->proximo->anterior != i )
            {
                return false;
            }
            if( i->anterior->proximo != i )
            {
                return false;
            }
        }
    }

    return true;
}

void LIS_ImprimirLista(LIS_Lista lista)
{
    std::cout << "Tamanho " << lista->tamanho << std::endl;
    for(LIS_No i = lista->cabeca->proximo; i != lista->cauda; i = i->proximo)
    {
        std::cout << "[" << i->valor << "]->";
    }
    std::cout << std::endl;
}

LIS_No LIS_CriarNo(int v)
{
    LIS_No no = (LIS_No) malloc( sizeof( tpNo ) );
    if( no == NULL )
    {
        return NULL;
    }

    no->proximo = NULL;

    no->valor = v;

    return no;
}

bool LIS_DestruirNo(LIS_No no)
{
    //free(no);
    delete no;
    return true;
}


